import express from "express";
import { ApolloServer, gql } from "apollo-server-express";
import typeDefs from "./schema/queries/Topic";
import resolvers from "./queries/resolvers";
import db from "./models";

const server = new ApolloServer({
  typeDefs: gql(typeDefs),
  resolvers,
  context: { db }
});

const app = express();
server.applyMiddleware({ app });

//save all file
app.use(express.static("public"));

db.sequelize.sync().then(() => {
  app.listen({ port: 4000 }, () =>
    console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
  );
});