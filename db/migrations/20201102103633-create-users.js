'use strict';
const { QueryTypes } = require('sequelize');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(`CREATE DOMAIN user_id AS TEXT CHECK(VALUE ~ '^[0-9a-z]{11}$')`);

    await queryInterface.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      uuid: {
        allowNull: false,
        type: "user_id"
      },
      username: {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
      admin: {
        defaultValue: false,
        type: Sequelize.BOOLEAN
      },
      blocked: {
        defaultValue: false,
        type: Sequelize.BOOLEAN
      },
      archived: {
        defaultValue: false,
        type: Sequelize.BOOLEAN
      },
      last_login: {
        allowNull: true,
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('users');
    await queryInterface.sequelize.query("DROP DOMAIN IF EXISTS user_id");
  }
};