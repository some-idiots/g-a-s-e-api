'use strict';

const defaultCategories = [
  "movie",
  "book",
  "food",
  "travel",
  "other"
];

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(`DROP TYPE IF EXISTS category_enum`);
    await queryInterface.sequelize.query(`CREATE TYPE category_enum AS ENUM (${defaultCategories.map(x => `'${x}'`).join(', ')})`);

    await queryInterface.createTable('topics', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      author_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'users',
          key: 'id'
        },
      },
      category: {
        allowNull: false,
        type: "category_enum"
      },
      title: {
        type: Sequelize.STRING(100),
        unique: true
      },
      slug: {
        type: Sequelize.STRING(200)
      },
      question: {
        type: Sequelize.STRING(100)
      },
      content: {
        type: Sequelize.STRING(1000)
      },
      image: {
        type: Sequelize.STRING(1000)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });

    await queryInterface.createTable('topic_items', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      author_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'users',
          key: 'id'
        },
      },
      category: {
        allowNull: false,
        type: "category_enum"
      },
      title: {
        type: Sequelize.STRING(100),
        unique: true
      },
      slug: {
        type: Sequelize.STRING(200)
      },
      content: {
        type: Sequelize.STRING(1000)
      },
      image: {
        type: Sequelize.STRING(1000)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });

    await queryInterface.createTable('topic_item_refs', {
      topic_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        references: {
          model: 'topics',
          key: 'id'
        },
      },
      item_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        references: {
          model: 'topic_items',
          key: 'id'
        },
      },
      author_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'users',
          key: 'id'
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('topic_item_refs');
    await queryInterface.dropTable('topics');
    await queryInterface.dropTable('topic_items');
    await queryInterface.sequelize.query(`DROP TYPE IF EXISTS category_enum`);
  }
};