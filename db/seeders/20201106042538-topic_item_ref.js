'use strict';

const fs = require("fs");
const prettier = require("prettier");
const slugify = require("slugify");
const { date, image, lorem, fake, random } = require("faker");

const jsonFile = `${__dirname}/04_topic_item_refs.json`;
const jsonUsersFile = `${__dirname}/01_users.json`;
const jsonTopicsFile = `${__dirname}/02_topics.json`;
const jsonItemsFile = `${__dirname}/03_items.json`;
function stringify(obj) {
  return prettier.format(JSON.stringify(obj), { parser: "json" });
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let topic_item_refs = fs.existsSync(jsonFile) ? require(jsonFile) : null;
    let items = fs.existsSync(jsonItemsFile) ? require(jsonItemsFile) : null;
    let topics = fs.existsSync(jsonTopicsFile) ? require(jsonTopicsFile) : null;
    let users = fs.existsSync(jsonUsersFile) ? require(jsonUsersFile) : null;

    if (!topic_item_refs) {
      console.log("Generating topic_item_refs.json...");
      const magicKeys = new Set();
  
      topic_item_refs = Array.from({ length: 4000 }).map((currentElem, index, arr) => {
        let user = users[Math.floor(Math.random() * users.length)];
        let author_id = user.id;

        let topic_id = random.arrayElement(topics).id;
        let item_id = random.arrayElement(items).id;
  
        let magicKey = `${topic_id}_${item_id}`;
        let createdAt = date.recent(365);
  
        // Ensures that the magicKey is unique
        while (magicKeys.has(magicKey)) {
          topic_id = random.arrayElement(topics).id;
          item_id = random.arrayElement(items).id;
          magicKey = `${topic_id}_${item_id}`;
        }
  
        magicKeys.add(magicKey);
  
        return {
          topic_id,
          item_id,
          author_id,
          createdAt: createdAt,
          updatedAt: createdAt,
        };
      });
  
      fs.writeFileSync(jsonFile, stringify(topic_item_refs), "utf8");
    }
    fs.writeFileSync(jsonFile, stringify(topic_item_refs), "utf8");

    return await queryInterface.bulkInsert('topic_item_refs', topic_item_refs, {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('topic_item_refs', null, {});
  }
};
