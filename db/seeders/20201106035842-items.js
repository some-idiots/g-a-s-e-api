'use strict';

const fs = require("fs");
const prettier = require("prettier");
const slugify = require("slugify");
const { date, image, lorem } = require("faker");

const defaultCategories = [
  "movie",
  "book",
  "food",
  "travel",
  "other"
];

const jsonFile = `${__dirname}/03_items.json`;
const jsonUsersFile = `${__dirname}/01_users.json`;
function stringify(obj) {
  return prettier.format(JSON.stringify(obj), { parser: "json" });
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let items = fs.existsSync(jsonFile) ? require(jsonFile) : null;
    let users = fs.existsSync(jsonUsersFile) ? require(jsonUsersFile) : null;

    if (!items) {
      console.log("Generating items.json...");
      const slugs = new Set();
  
      items = Array.from({ length: 1000 }).map((currentElem, index, arr) => {
        const category = defaultCategories[Math.floor(Math.random() * defaultCategories.length)];
        const user = users[Math.floor(Math.random() * users.length)];
        const author_id = user.id;
  
        const title = lorem.sentence();
        let slug = slugify(title);
        const content = lorem.sentences(4);
        const imageUrl = image.imageUrl(320,640);
        const createdAt = date.recent(365);
  
        // Ensures that the slug is unique
        while (slugs.has(slug)) {
          slug = `${slugify(title)}-${random.number(1000)+index}`;
        }
  
        slugs.add(slug);
  
        return {
          id: index,
          category,
          author_id,
          title,
          slug,
          content,
          image: imageUrl,
          createdAt: createdAt,
          updatedAt: createdAt,
        };
      });
  
      fs.writeFileSync(jsonFile, stringify(items), "utf8");
    }
  
    fs.writeFileSync(jsonFile, stringify(items), "utf8");
    return await queryInterface.bulkInsert('topic_items', items, {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('topic_items', null, {});
  }
};
