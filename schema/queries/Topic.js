export default `
  type User {
    id: ID!
    firstName: String!
    lastName: String!
    posts: [Topic!]!
  }
  type Topic {
    id: ID!
    title: String
    content: String!
    authorId: ID!
    author: User!
  }
  type Query {
    posts: [Topic!]!
    post(id: ID!): Topic
    author(id: ID!): User
    authors: [User!]!
  }
  type Mutation {
    createTopic(title: String, content:String!, authorId: ID!): Topic!
    updateTopic(id: ID!, title: String, content:String!): [Int!]!
    deleteTopic(id: ID!): Int!
  }
`;