export default {
    User: {
        topics: (parent, args, context, info) => parent.getTopics(),
    },
    Topic: {
        user: (parent, args, context, info) => parent.getUser(),
    },
    Query: {
        topics: (parent, args, { db }, info) => db.topic.findAll(),
        users: (parent, args, { db }, info) => db.user.findAll(),
        topic: (parent, { id }, { db }, info) => db.topic.findByPk(id),
        user: (parent, { id }, { db }, info) => db.user.findByPk(id)
    },
    Mutation: {
        createTopic: (parent, { title, content, userId }, { db }, info) =>
            db.topic.create({
                title: title,
                content: content,
                userId: userId
            }),
        updateTopic: (parent, { title, content, id }, { db }, info) =>
            db.topic.update({
                title: title,
                content: content
            },
                {
                    where: {
                        id: id
                    }
                }),
        deleteTopic: (parent, { id }, { db }, info) =>
            db.topic.destroy({
                where: {
                    id: id
                }
            })
    }
};